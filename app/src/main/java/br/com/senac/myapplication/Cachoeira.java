package br.com.senac.myapplication;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by sala302b on 06/02/2018.
 */

public class Cachoeira implements Serializable{
    private int id;
    private String nome;
    private String informacoes;
    private float classificacoes;
    private String imagem;
    private String endereco;
    private String telefone;
    private String email;
    private String site;


    public Cachoeira(){}

    public Cachoeira(int id, String nome, String informacoes, float classificacoes, String endereco, String telefone, String email, String site, String imagem){
        this.id = id;
        this.nome = nome;
        this.informacoes = informacoes;
        this.classificacoes = classificacoes;
        this.imagem = imagem;
        this.endereco = endereco;
        this.telefone = telefone;
        this.email = email;
        this.site = site;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getInformacoes() {
        return informacoes;
    }

    public void setInformacoes(String informacoes) {
        this.informacoes = informacoes;
    }

    public float getClassificacoes() {
        return classificacoes;
    }

    public void setClassificacoes(float classificacoes) {
        this.classificacoes = classificacoes;
    }

        @Override
    public String toString() {
        return this.getNome();
    }
}
