package br.com.senac.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sala304b on 27/02/2018.
 */

public class CachoeiraDAO extends SQLiteOpenHelper {

    private static final String DATABASE = "SQLite";
    private static final int VERSAO = 1;

    public CachoeiraDAO(Context context) {
        super(context, DATABASE, null, VERSAO);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String ddl = "CREATE TABLE Cachoeiras(id INTEGER PRIMARY KEY , " +
                "nome TEXT NOT NULL , " +
                "informacoes TEXT , " +
                "classificacoes REAL , " +
                "endereco TEXT , " +
                "telefone TEXT , " +
                "email TEXT , " +
                "site TEXT , " +
                "imagem TEXT ) ; ";

        db.execSQL(ddl);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String ddl = "DROP TABLE IF EXISTS Cachoeiras";
        db.execSQL(ddl);
        this.onCreate(db);
    }

    public void salvar(Cachoeira cachoeira) {
        ContentValues values = new ContentValues();
        values.put("nome", cachoeira.getNome());
        values.put("informacoes", cachoeira.getInformacoes());
        values.put("classificacoes", cachoeira.getClassificacoes());
        values.put("endereco", cachoeira.getEndereco());
        values.put("telefone", cachoeira.getTelefone());
        values.put("email", cachoeira.getEmail());
        values.put("site", cachoeira.getSite());
        values.put("imagem", cachoeira.getImagem());

        if (cachoeira.getId() == 0) {
            getWritableDatabase().insert("Cachoeiras", null, values);
        } else {
            getWritableDatabase().update("Cachoeiras", values, "id=" + cachoeira.getId(), null);
        }


        System.out.println("SALVAR_OK");
    }


    public List<Cachoeira> getLista() {
        List<Cachoeira> lista = new ArrayList<>();
        String colunas[] = {"id", "nome", "informacoes", "classificacoes", "endereco", "telefone", "email", "site", "imagem"};

        Cursor cursor = getWritableDatabase().query("Cachoeiras", colunas, null, null, null, null, null);

        while (cursor.moveToNext()) {
            Cachoeira cachoeira = new Cachoeira();
            cachoeira.setId(cursor.getInt(0));
            cachoeira.setNome(cursor.getString(1));
            cachoeira.setInformacoes(cursor.getString(2));
            cachoeira.setClassificacoes(cursor.getFloat(3));
            cachoeira.setEndereco(cursor.getString(4));
            cachoeira.setTelefone(cursor.getString(5));
            cachoeira.setEmail(cursor.getString(6));
            cachoeira.setSite(cursor.getString(7));
            cachoeira.setImagem(cursor.getString(8));
            lista.add(cachoeira);


            System.out.println("GETLISTA_OK");

        }
        return lista;
    }


}
