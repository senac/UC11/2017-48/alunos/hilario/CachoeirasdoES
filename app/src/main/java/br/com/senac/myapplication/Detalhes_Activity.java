package br.com.senac.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import static br.com.senac.myapplication.MainActivity.IMAGEM;
import static br.com.senac.myapplication.MainActivity.imagem;

public class Detalhes_Activity extends AppCompatActivity {

    public static final String CACHOEIRA = "cachoeira";

    private TextView txtNomeCachoeira;
    private TextView txtInformacoes;
    private RatingBar ratingBarDetalhes;
    private ImageView imageView;
    private TextView txtEndereco;
    private TextView txtTelefone;
    private TextView txtEmail;
    private TextView txtSite;


    private Cachoeira cachoeira;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_);

        txtNomeCachoeira = findViewById(R.id.txtNomeCachoeira);
        txtInformacoes = findViewById(R.id.txtInformacoes);
        ratingBarDetalhes = findViewById(R.id.ratingBarDetalhes);
        imageView = findViewById(R.id.imageView);
        txtEndereco = findViewById(R.id.txtEndereco);
        txtTelefone = findViewById(R.id.txtTelefone);
        txtEmail = findViewById(R.id.txtEmail);
        txtSite = findViewById(R.id.txtsite);

        cachoeira = (Cachoeira) getIntent().getSerializableExtra(CACHOEIRA);

        txtNomeCachoeira.setText(cachoeira.getNome());
        txtInformacoes.setText(cachoeira.getInformacoes());
        ratingBarDetalhes.setRating(cachoeira.getClassificacoes());

        System.out.println(cachoeira.getImagem() + "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

        if (cachoeira.getImagem() != null) {
            Bitmap foto = BitmapFactory.decodeFile(cachoeira.getImagem());
            Bitmap fotoReduzida = Bitmap.createScaledBitmap(foto, 200, 200, true);
            imageView.setImageBitmap(fotoReduzida);
        }


//        imageView.setImageBitmap(imagem);
        txtEndereco.setText(cachoeira.getEndereco());
        txtTelefone.setText(cachoeira.getTelefone());
        txtEmail.setText(cachoeira.getEmail());
        txtSite.setText(cachoeira.getSite());


    }
}
