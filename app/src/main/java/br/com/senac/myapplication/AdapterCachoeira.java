package br.com.senac.myapplication;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sala302b on 16/02/2018.
 */

public class AdapterCachoeira extends BaseAdapter {

    private List<Cachoeira> lista;
    private Activity contexto;

    public AdapterCachoeira(Activity contexto, List lista) {
        this.contexto = contexto;
        this.lista = lista;

    }


    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int indice) {
        return this.lista.get(indice);
    }

    @Override
    public long getItemId(int id) {
        int posicao = 0;

        for (int i = 0; i < this.lista.size(); i++) {
            if (this.lista.get(i).getId() == id) {
                posicao = i;
                break;
            }
        }
        return posicao;
    }   ///Nao precisa

    @Override
    public View getView(int posicao, View convertview, ViewGroup parent) {

        View view = contexto.getLayoutInflater().inflate(R.layout.cachoeira_lista, parent, false);

        TextView textView = view.findViewById(R.id.nomeCachoeiraLista);
        ImageView imageView = view.findViewById(R.id.fotoLista);
        RatingBar ratingBar = view.findViewById(R.id.ratingBarLista);

        Cachoeira cachoeira = this.lista.get(posicao);

        textView.setText(cachoeira.getNome());


        if (cachoeira.getImagem() != null) {
            System.out.println(cachoeira.getImagem() + "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!@#!@#!@#!@#!@#");
            Bitmap foto = BitmapFactory.decodeFile(cachoeira.getImagem());
            System.out.println(foto + "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!@#!@#!@#!@#!@#");

            Bitmap fotoReduzida = Bitmap.createScaledBitmap(foto, 200, 200, true);

            imageView.setImageBitmap(fotoReduzida);
        }


//        Bitmap foto = BitmapFactory.decodeFile(cachoeira.getImagem());
//        imageView.setImageBitmap(foto);

        ratingBar.setRating(cachoeira.getClassificacoes());

        return view;
    }


}
