package br.com.senac.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static br.com.senac.myapplication.MainActivity.IMAGEM;

public class Novo_Activity extends AppCompatActivity {

    public static final String CACHOEIRA = "cachoeira";
    static final int REQUEST_TAKE_PHOTO = 1;



    private EditText nomeCachoeira;
    private EditText textInformacoes;
    private RatingBar ratingBarNovo;
    private Button btnSalvar;
    private Button btnVoltar;
    private ImageButton imageButtonFoto;
    private static Bitmap photo;
    private EditText textEmail;
    private EditText textTelefone;
    private EditText textEndereco;
    private EditText textSite;
    private Cachoeira cachoeira;
    private CachoeiraDAO dao;
    private boolean cachoeiraNova;
    String mCurrentPhotoPath;
    List<String> listaCamposRequeridos = new ArrayList<>();



    //Tirar fotos
    public static void setPhoto(Bitmap photo) {
        Novo_Activity.photo = photo;
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Toast.makeText(this, "ERRO AO TIRAR FOTO", Toast.LENGTH_SHORT).show();
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                //  takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = getExternalFilesDir(getApplication().getFilesDir().toString());
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;

    }

    //Validar dados

    private boolean isPreenchido(String string) {
        return (!string.isEmpty());
    }
    public List<String> getListaCamposRequeridos() {
        return listaCamposRequeridos;
    }
    public boolean validarDados(Cachoeira cachoeira) {

        if (!isPreenchido(cachoeira.getNome())) {
            listaCamposRequeridos.add("Nome");
        }

        if (!isPreenchido(cachoeira.getInformacoes())) {
            listaCamposRequeridos.add("Informacoes");
        }

        if (!isPreenchido(cachoeira.getEmail())) {
            listaCamposRequeridos.add("Email");
        }

        if (!isPreenchido(cachoeira.getTelefone())) {
            listaCamposRequeridos.add("Telefone");
        }

        if (!isPreenchido(cachoeira.getEndereco())) {
            listaCamposRequeridos.add("Endereco");
        }

        if (!isPreenchido(cachoeira.getSite())) {
            listaCamposRequeridos.add("Site");
        }

        if (listaCamposRequeridos.size() > 0) {
            for (String s : listaCamposRequeridos) {
                System.out.println(s);
            }
            listaCamposRequeridos.clear();
            return false;
        }
        return true;

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK && data != null) {
            photo = (Bitmap) data.getExtras().get("data");
            imageButtonFoto.setImageBitmap(photo);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_);


        nomeCachoeira = findViewById(R.id.nomeCachoeira);
        textInformacoes = findViewById(R.id.textInformacoes);
        ratingBarNovo = findViewById(R.id.ratingBarNovo);
        btnSalvar = findViewById(R.id.btnSalvar);
        btnVoltar = findViewById(R.id.btnVoltar);
        imageButtonFoto = findViewById(R.id.imageButtonFoto);
        textEndereco = findViewById(R.id.enderecoCachoeira);
        textEmail = findViewById(R.id.emailCachoeira);
        textTelefone = findViewById(R.id.telCachoeira);
        textSite = findViewById(R.id.siteCachoeira);


//        try {


        Intent intent = getIntent();

        cachoeira = (Cachoeira) intent.getSerializableExtra(MainActivity.CACHOEIRA);

        if (cachoeira == null) {
            cachoeira = new Cachoeira();
            cachoeiraNova = true;


        } else {

            nomeCachoeira.setText(cachoeira.getNome().toString());
            textInformacoes.setText(cachoeira.getInformacoes().toString());
            ratingBarNovo.setRating(cachoeira.getClassificacoes());
            textEndereco.setText(cachoeira.getEndereco().toString());
            textEmail.setText(cachoeira.getEmail().toString());
            textTelefone.setText(cachoeira.getTelefone().toString());
            textSite.setText(cachoeira.getSite().toString());
           // imageButtonFoto.setImageBitmap(cachoeira.getImagem());

        }

        imageButtonFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();
            }
        });


        btnSalvar.setOnClickListener(new View.OnClickListener()

        {

            @Override
            public void onClick(View view) {


                String nome = nomeCachoeira.getText().toString();
                String informacoes = textInformacoes.getText().toString();
                float classificacoes = ratingBarNovo.getRating();
                String endereco = textEndereco.getText().toString();
                String email = textEmail.getText().toString();
                String telefone = textTelefone.getText().toString();
                String site = textSite.getText().toString();
                String imagem = mCurrentPhotoPath;


                cachoeira.setNome(nome);
                cachoeira.setInformacoes(informacoes);
                cachoeira.setClassificacoes(classificacoes);
                cachoeira.setEndereco(endereco);
                cachoeira.setEmail(email);
                cachoeira.setTelefone(telefone);
                cachoeira.setSite(site);
                cachoeira.setImagem(imagem);


                //validarDados(cachoeira);

//                try {
                dao = new CachoeiraDAO(Novo_Activity.this);
                dao.salvar(cachoeira);
                dao.close();


//                    if (validarDados(cachoeira) == true) {
                if (1 == 1) {


                    Intent intent = new Intent();
                    intent.putExtra(CACHOEIRA, cachoeira);
                    setResult(RESULT_OK, intent); //seta um resultado antes de voltar para a activity anterior

                    cachoeiraNova = false;

                    finish(); //volta para a activity anterior

                } else {
                    // throw new Exception(e);
                    Toast.makeText(Novo_Activity.this, "DADOS INVÁLIDOSXX", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder builder = new AlertDialog.Builder(Novo_Activity.this);
                    builder.setTitle("Erro de Validação").setMessage("Os campos:\n" + getListaCamposRequeridos().toString() + "Sao necessarios para o cadastro.").show();

                }

//                } catch (Exception ex) {
//                    Toast.makeText(Novo_Activity.this, "Erro ao Salvar.", Toast.LENGTH_LONG).show();
//                }


            }
        });
//        } catch (Exception ex) {
//
//            System.out.println("ENTREI NA EXCEÇÃO!!!");
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Erro de Validação").setMessage("Os campos:\n" + listaCamposRequeridos + "Sao necessarios para o cadastro.").show();
//
//
//        }

    }
}


