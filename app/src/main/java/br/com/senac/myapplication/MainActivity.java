package br.com.senac.myapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    public static final int MAIN_TO_NOVO = 1;
    public static final int DETALHES = 2;
    public static final String IMAGEM = "AA";
    public static final String CACHOEIRA = "cachoeira";
    private static final int EDITAR = 3;

    private Cachoeira cachoeiraSelecionada;
    private ListView listView;
    private ArrayList<Cachoeira> listaCachoeiras = new ArrayList<>();
    private ArrayAdapter<Cachoeira> adapter;
    private AdapterCachoeira adapterCachoeira;

    private CachoeiraDAO dao;
    List<Cachoeira> lista = new ArrayList<>();

    public static Bitmap imagem;

    public Bitmap getImagem() {
        return imagem;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        cachoeiraSelecionada = (Cachoeira) adapterCachoeira.getItem(info.position);


        MenuItem menuSite = menu.add("Visitar Site");
        Intent intentSite = new Intent(Intent.ACTION_DEFAULT);
        String site = cachoeiraSelecionada.getSite();
        if (!site.startsWith("http://")) {
            site = "http://" + site;
        }
        intentSite.setData(Uri.parse(site));
        menuSite.setIntent(intentSite);


        MenuItem menuLigar = menu.add("Ligar");
        Intent intentLigar = new Intent(Intent.ACTION_DIAL);
        String telefone = cachoeiraSelecionada.getTelefone();
        intentLigar.setData(Uri.parse("tel:" + telefone));
        menuLigar.setIntent(intentLigar);
//

        MenuItem menuEmail = menu.add("Enviar Email");
        String email = cachoeiraSelecionada.getEmail();
        Intent intentEmail = new Intent(Intent.ACTION_SENDTO);
        intentEmail.setData(Uri.parse("Email: " + email));
        menuEmail.setIntent(intentEmail);
//

        MenuItem menuEditar = menu.add("Editar");
        Intent intentEditar = new Intent(MainActivity.this, Novo_Activity.class);
        intentEditar.putExtra(CACHOEIRA, cachoeiraSelecionada);
        menuEditar.setIntent(intentEditar);

        menu.add("Como Chegar");

    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }


    public void novo(MenuItem item) {
        Intent intent = new Intent(MainActivity.this, Novo_Activity.class);
        intent.putExtra("listacachoeiras", (Serializable) listaCachoeiras);
        startActivityForResult(intent, MAIN_TO_NOVO);
    }

    public void sobre(MenuItem item) {
        Toast versao = Toast.makeText(MainActivity.this, "Cachoeiras do ES\nv1.0b", Toast.LENGTH_LONG);
        versao.show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listaCachoeiras);
        adapterCachoeira = new AdapterCachoeira(this, listaCachoeiras);
        listView.setAdapter(adapterCachoeira);
        registerForContextMenu(listView);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posicao, long posicaoItemAdapter) {
                cachoeiraSelecionada = (Cachoeira) adapterCachoeira.getItem(posicao);
                Intent intent = new Intent(MainActivity.this, Detalhes_Activity.class);
                intent.putExtra(CACHOEIRA, cachoeiraSelecionada);
//                imagem = cachoeiraSelecionada.getImagem();
//                intent.putExtra(IMAGEM, imagem);
                startActivityForResult(intent, DETALHES);

            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MainActivity.this, "LONGCLICK", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        dao = new CachoeiraDAO((this));
        lista = dao.getLista();
        dao.close();

        /* criando adapter */

        adapterCachoeira = new AdapterCachoeira(this, lista);

        /* definindo o adapter do listview */

        listView.setAdapter(adapterCachoeira);

    }


}
